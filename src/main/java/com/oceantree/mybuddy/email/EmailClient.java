package com.oceantree.mybuddy.email;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@EnableAsync
@Component
public class EmailClient {

	private static Log log = LogFactory.getLog(EmailClient.class.getName());

	@Value("${email.server.url}")
	private String emailServerUrl;
	
	private String templateBasedUrl;
	private String templateBasedUrlBCC;
	private String customEmailUrl;
	
	public static String SYSTEM_EMAIL = "jobs@thepridecircle.com";
	
	@PostConstruct
	public void init(){
		templateBasedUrl = emailServerUrl +"/sendTemplateMail";
		templateBasedUrlBCC = emailServerUrl +"/sendTemplateMailBCC";
		customEmailUrl = emailServerUrl + "/sendMail"; // TODO change
	}
	
	//TDOO uncomment send email
	private void sendEmailByTemplate(EmailBody emailBody) {
        RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForEntity(templateBasedUrl, emailBody, String.class);
	}
	
	@Async
	public void sendPaymentFailuer(EmailBody emailBody) {
        RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForEntity(customEmailUrl, emailBody, String.class);
	}

	private RestTemplate getRestTemplate() {
		return new RestTemplate();//RestTemplate(getClientHttpRequestFactory());
	}


	
	@Async
	public void sendResumeConfirmation(String userEmail) {
		if(StringUtils.isEmpty(userEmail)) return;
		try {
	        RestTemplate restTemplate = new RestTemplate();
	        EmailBody eb = new EmailBody();
	        StringBuilder sb = new StringBuilder();
	        sb.append("Hi").append("\n").
	        append("Thank you for uploading/updating your resume. The team will match you with relevant openings and get in touch with you. For any queries please write to jobs@thepridecircle.com")
	        .append("\n").append("Thanks");
	        eb.setBody(sb.toString());
	        eb.setSubject("Resume Upload Confirmation - Pride Circle");
	        eb.getToList().add(userEmail);
	        eb.getToList().add(SYSTEM_EMAIL);
	       // eb.setToList(AppConstants.SYSTEM_EMAIL);
	        log.info("sending email .... "  + eb);
			restTemplate.postForEntity(customEmailUrl, eb, String.class);			
		}catch(Exception ex) {
			log.error("Error sending email ", ex);
		}
	}

		//Override timeouts in request factory
	private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory()
	{
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
	                      = new HttpComponentsClientHttpRequestFactory();
	    //Connect timeout
	    clientHttpRequestFactory.setConnectTimeout(1000);
	     
	    //Read timeout
	   // clientHttpRequestFactory.setReadTimeout(10_000);
	    return clientHttpRequestFactory;
	}
}
