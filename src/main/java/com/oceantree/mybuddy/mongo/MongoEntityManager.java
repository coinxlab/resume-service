package com.oceantree.mybuddy.mongo;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class MongoEntityManager {
	
	private static Log log = LogFactory.getLog(MongoEntityManager.class.getName());
	
	@Autowired
	private MongoOperations operations;
	

	public void save(Object object) {
		operations.save(object);
	}
	
	public  Object findById(String id, Class<?> className) {
		return operations.findById(id, className);
	}
	
	public  List<?> findAll( Class<?> className) {
		return operations.findAll( className);
	}
	
	public void update(Query query, Update update, Class<?> className) {
		operations.updateMulti(query, update, className);
	}
}
