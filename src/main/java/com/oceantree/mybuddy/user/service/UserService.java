package com.oceantree.mybuddy.user.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oceantree.mybuddy.common.Result;
import com.oceantree.mybuddy.mongo.MongoEntityManager;
import com.oceantree.mybuddy.mongo.MongoQueryManager;
import com.oceantree.mybuddy.user.model.PCUser;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path="/user")
public class UserService {

private static Log log = LogFactory.getLog(UserService.class.getName());
	
	@Autowired
	private MongoQueryManager mongoQueryMgr;
	
	@Autowired
	private MongoEntityManager mongoEntityMgr ;
	
	@PostMapping(value="/save")
	public Result saveUser(@RequestBody PCUser user) {
		mongoEntityMgr.save(user);
		log.info("project saved ...");
		return new Result();
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping(value="/query")
	public List<PCUser> searchUser(@RequestBody Map<String, String> queryMap) {
		return ((List<PCUser>) mongoQueryMgr.executeQuery(queryMap, PCUser.class));

	}
	
	@RequestMapping(value="/find-all-users" , method = { RequestMethod.GET, RequestMethod.POST })
	public List<?> findAllUser() {
			return mongoEntityMgr.findAll(PCUser.class);
	}
}
