package com.oceantree.mybuddy.user.model;

import org.joda.time.DateTime;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.oceantree.mybuddy.user.UserType;

@EntityScan
@Document
public class PCUser {

	@Id
    public String id;

	@Indexed(unique=true)
	private String userId; // email-id as user id
	
	@Indexed
	private String userType = UserType.USER.name();
	
	// audit item
	@Version
	private Long version;

	@CreatedDate
	private DateTime createdDate;

	@LastModifiedDate
	private DateTime lastModifiedDate;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getVersion() {
		return version;
	}
	public DateTime getCreatedDate() {
		return createdDate;
	}
	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}
	
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userType=" + userType + "]";
	}
	
	
}
