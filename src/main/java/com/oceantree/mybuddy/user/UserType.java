package com.oceantree.mybuddy.user;

public enum UserType {

	USER,
	ADMIN
}
