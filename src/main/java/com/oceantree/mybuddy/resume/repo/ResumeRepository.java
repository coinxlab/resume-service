package com.oceantree.mybuddy.resume.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.oceantree.mybuddy.resume.model.Resume;

public interface ResumeRepository extends MongoRepository<Resume, String> {

	public Resume findByUserId(String userId);
}
