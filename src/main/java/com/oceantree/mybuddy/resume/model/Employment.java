package com.oceantree.mybuddy.resume.model;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;

public class Employment {

	private String currentEmployer;
	
	@Indexed
	private String totalExperience; // double to string
	private String summary;
	
	@Indexed
	private String industry;
	
	@Indexed
	private String functionalArea;
	
	@Indexed
	private String role;
	private List<String> prefWorkLocations;
	private String jobType;
	private String empType;
	private boolean isAccomodationRequired;
	private String accomodationDetails;
	private String expectedCTC;
	
	@Indexed
	private String currentCtc;
	private String designation;
	
	private String appliedJobs;
	
	public String getCurrentEmployer() {
		return currentEmployer;
	}
	public void setCurrentEmployer(String currentEmployer) {
		this.currentEmployer = currentEmployer;
	}
	
	public String getTotalExperience() {
		return totalExperience;
	}
	public void setTotalExperience(String totalExperience) {
		this.totalExperience = totalExperience;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	
	public String getFunctionalArea() {
		return functionalArea;
	}
	public void setFunctionalArea(String functionalArea) {
		this.functionalArea = functionalArea;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<String> getPrefWorkLocations() {
		return prefWorkLocations;
	}
	public void setPrefWorkLocations(List<String> prefWorkLocations) {
		this.prefWorkLocations = prefWorkLocations;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getEmpType() {
		return empType;
	}
	public void setEmpType(String empType) {
		this.empType = empType;
	}
	public boolean isAccomodationRequired() {
		return isAccomodationRequired;
	}
	public void setAccomodationRequired(boolean isAccomodationRequired) {
		this.isAccomodationRequired = isAccomodationRequired;
	}
	public String getAccomodationDetails() {
		return accomodationDetails;
	}
	public void setAccomodationDetails(String accomodationDetails) {
		this.accomodationDetails = accomodationDetails;
	}
	public String getExpectedCTC() {
		return expectedCTC;
	}
	public void setExpectedCTC(String expectedCTC) {
		this.expectedCTC = expectedCTC;
	}
	public String getCurrentCtc() {
		return currentCtc;
	}
	public void setCurrentCtc(String currentCtc) {
		this.currentCtc = currentCtc;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getAppliedJobs() {
		return appliedJobs;
	}
	public void setAppliedJobs(String appliedJobs) {
		this.appliedJobs = appliedJobs;
	}
}
