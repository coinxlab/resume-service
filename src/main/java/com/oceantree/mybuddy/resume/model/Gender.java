package com.oceantree.mybuddy.resume.model;

import org.springframework.data.mongodb.core.index.Indexed;

public class Gender {

	@Indexed
	private String identity;
	@Indexed
	private String sexualOrientation;
	@Indexed
	private String outAtWork;
	
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getSexualOrientation() {
		return sexualOrientation;
	}
	public void setSexualOrientation(String sexualOrientation) {
		this.sexualOrientation = sexualOrientation;
	}
	public String getOutAtWork() {
		return outAtWork;
	}
	public void setOutAtWork(String outAtWork) {
		this.outAtWork = outAtWork;
	}
	
	
}
