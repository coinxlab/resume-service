package com.oceantree.mybuddy.resume.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@EntityScan
@Document
public class Resume {
	
	@Id
    public String id;

	@NotNull(message = "userId name must not be null")
	@Indexed(unique=true)
	private String userId;
		
	private Personal personal;
	
	@Indexed
	private List<String> skills;
	
	private Education education;
	private Employment employment;
	
	private long lastModifiedTimeInMilli;
	
	private String profileUrl;
	
	private ResumeFile resumeFile;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Personal getPersonal() {
		return personal;
	}
	public void setPersonal(Personal personal) {
		this.personal = personal;
	}
	public List<String> getSkills() {
		return skills;
	}
	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	public Education getEducation() {
		return education;
	}
	public void setEducation(Education education) {
		this.education = education;
	}
	public Employment getEmployment() {
		return employment;
	}
	public void setEmployment(Employment employment) {
		this.employment = employment;
	}
	
	public long getLastModifiedTimeInMilli() {
		return lastModifiedTimeInMilli;
	}
	public void setLastModifiedTimeInMilli(long lastModifiedTimeInMilli) {
		this.lastModifiedTimeInMilli = lastModifiedTimeInMilli;
	}
	
	public String getProfileUrl() {
		return profileUrl;
	}
	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
	
	
	public ResumeFile getResumeFile() {
		return resumeFile;
	}
	public void setResumeFile(ResumeFile resumeFile) {
		this.resumeFile = resumeFile;
	}
	@Override
	public String toString() {
		return "Resume [id=" + id + ", userId=" + userId + ", personal=" + personal + ", skills=" + skills + ", education=" + education + ", employment=" + employment + ", lastModifiedTimeInMilli="
				+ lastModifiedTimeInMilli + "]";
	}
	
}
