package com.oceantree.mybuddy.resume.model;

import org.springframework.data.mongodb.core.index.Indexed;

public class Personal {

	@Indexed
	private String legalName;
	
	@Indexed
	private String preferredName;
	private String pronoun;
	private String email;
	private String phone;
	
	@Indexed
	private String sex;
	
	@Indexed
	private String dateOfBirth;
	
	@Indexed
	private String emergencyContact;
	
	private String linkedlnUrl;
	private String twitterUrl;
	
	@Indexed
	private String currentLocation;
	
	@Indexed
	private String identity;
	@Indexed
	private String sexualOrientation;
	@Indexed
	private String outAtWork;
	

	public String getLegalName() {
		return legalName;
	}
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	
	public String getPreferredName() {
		return preferredName;
	}
	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}
	public String getPronoun() {
		return pronoun;
	}
	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getEmergencyContact() {
		return emergencyContact;
	}
	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}
	public String getLinkedlnUrl() {
		return linkedlnUrl;
	}
	public void setLinkedlnUrl(String linkedlnUrl) {
		this.linkedlnUrl = linkedlnUrl;
	}
	public String getTwitterUrl() {
		return twitterUrl;
	}
	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}
	public String getCurrentLocation() {
		return currentLocation;
	}
	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getSexualOrientation() {
		return sexualOrientation;
	}
	public void setSexualOrientation(String sexualOrientation) {
		this.sexualOrientation = sexualOrientation;
	}
	public String getOutAtWork() {
		return outAtWork;
	}
	public void setOutAtWork(String outAtWork) {
		this.outAtWork = outAtWork;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	
}
