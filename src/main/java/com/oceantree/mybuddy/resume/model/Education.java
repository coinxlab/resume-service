package com.oceantree.mybuddy.resume.model;

public class Education {

	private String highestQualification;
	private String Course;
	private String specialization;
	private String college;
	private String institute;
	private String courseType;
	private String yearOfPassing;
	private String projects;
	private String references;
	private String marks;
	private String describeProject;
	private String language;
	private String anyCertifications;
	
	public String getHighestQualification() {
		return highestQualification;
	}
	public void setHighestQualification(String highestQualification) {
		this.highestQualification = highestQualification;
	}
	public String getCourse() {
		return Course;
	}
	public void setCourse(String course) {
		Course = course;
	}
	public String getSpecialization() {
		return specialization;
	}
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	
	public String getCollege() {
		return college;
	}
	public void setCollege(String college) {
		this.college = college;
	}
	public String getCourseType() {
		return courseType;
	}
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	public String getYearOfPassing() {
		return yearOfPassing;
	}
	public void setYearOfPassing(String yearOfPassing) {
		this.yearOfPassing = yearOfPassing;
	}
	public String getProjects() {
		return projects;
	}
	public void setProjects(String projects) {
		this.projects = projects;
	}
	public String getReferences() {
		return references;
	}
	public void setReferences(String references) {
		this.references = references;
	}
	public String getMarks() {
		return marks;
	}
	public void setMarks(String marks) {
		this.marks = marks;
	}
	public String getDescribeProject() {
		return describeProject;
	}
	public void setDescribeProject(String describeProject) {
		this.describeProject = describeProject;
	}
	public String getInstitute() {
		return institute;
	}
	public void setInstitute(String institute) {
		this.institute = institute;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getAnyCertifications() {
		return anyCertifications;
	}
	public void setAnyCertifications(String anyCertifications) {
		this.anyCertifications = anyCertifications;
	}
	
}
