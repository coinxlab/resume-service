package com.oceantree.mybuddy.resume.service;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oceantree.mybuddy.common.QueryParamTO;
import com.oceantree.mybuddy.email.EmailClient;
import com.oceantree.mybuddy.error.AppException;
import com.oceantree.mybuddy.mongo.MongoQueryManager;
import com.oceantree.mybuddy.resume.model.Resume;
import com.oceantree.mybuddy.resume.repo.ResumeRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path="/resume-mgr")
public class ResumeManager {

    private static Log log = LogFactory.getLog(ResumeManager.class.getName());

	@Autowired
	private MongoQueryManager mongoQueryMgr;
	
	@Autowired
	private ResumeRepository resumeRepository;
	
	@Autowired
	private EmailClient ec ;
	
	@PostMapping(value="/save-update")
	public Resume saveProject(@RequestBody Resume resume) throws AppException {	
		Resume rs = new Resume();
		try {
			 rs = resumeRepository.save(resume);
		}catch(Exception ex) {
			log.error("Error uploading logs...", ex);
			log.info("Retrying to save the resume again ...." + resume);
			Resume oldResume = findByUserId(resume.getUserId());
			if(oldResume != null) {
				oldResume.setEducation(resume.getEducation());
				oldResume.setEmployment(resume.getEmployment());
				oldResume.setPersonal(resume.getPersonal());
				oldResume.setProfileUrl(resume.getProfileUrl());
				oldResume.setResumeFile(resume.getResumeFile());
				oldResume.setSkills(resume.getSkills());
				oldResume.setLastModifiedTimeInMilli(Calendar.getInstance().getTimeInMillis());
				
				rs = resumeRepository.save(resume);
				log.info("Resume saved  in retry...");
			}
		}
		log.info("resume updated ..." + resume);
		log.info("sending email ...");
		if(rs.getPersonal() != null) {
		ec.sendResumeConfirmation(rs.getPersonal().getEmail());
		}
		
		return rs;
	}
	
	@RequestMapping(value = "/generic-query", method = { RequestMethod.POST })
	public List<Resume> searchResume(@RequestBody QueryParamTO paramTO) throws AppException {
		Map<String, String> queryMap = paramTO.getQueryMap();
		if(queryMap.size() == 0) {
			return resumeRepository.findAll();
		}
		if(paramTO.getPageNum() == null || paramTO.getPageNum() <= 0) {
			paramTO.setPageNum(null);
			paramTO.setPageSize(null);
		}

		List<Criteria> criteriaList = mongoQueryMgr.createLikeCriteriaList(queryMap);
		
		Query query = mongoQueryMgr.createQuery(criteriaList);

		@SuppressWarnings("unchecked")
		List<Resume> results = (List<Resume>) mongoQueryMgr.executeQuery(query, Resume.class, "lastModifiedTimeInMilli", paramTO.getPageNum(),
				paramTO.getPageSize());
		log.info("result size retruned : " + results.size());

		return results;
	}
	@RequestMapping(value = "/find-by-userId", method = { RequestMethod.GET })
	public Resume findByUserId(@RequestParam String userId) throws AppException {
		return resumeRepository.findByUserId(userId);
	}
}
