package com.oceantree.mybuddy.common;

/**
 * The aim of the class is return  success/error, for now its been used for success only
 * @author sinhanil
 *
 */
public class Result {

	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAILUER = "FAIL";
	
	private String message;
	private String status;
	
	public Result() {
		this.status = STATUS_SUCCESS;
	}
	
	public Result(String status) {
		this.status = status;
	}
	public Result(String status , String message) {
		this.message = message;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
		
	
}
