package com.oceantree.mybuddy.error;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AppException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Log log = LogFactory.getLog(AppException.class.getName());

	public AppException(String msg) {
		super(msg);
		log.error(msg);
	}
	
	public AppException(String msg, Exception ex) {
		super(msg, ex);
		log.error(msg,ex);
	}
}
