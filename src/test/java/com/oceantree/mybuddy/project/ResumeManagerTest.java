package com.oceantree.mybuddy.project;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.oceantree.mybuddy.common.QueryParamTO;
import com.oceantree.mybuddy.error.AppException;
import com.oceantree.mybuddy.resume.model.Gender;
import com.oceantree.mybuddy.resume.model.Personal;
import com.oceantree.mybuddy.resume.model.Resume;
import com.oceantree.mybuddy.resume.service.ResumeManager;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
public class ResumeManagerTest {
	
	@Autowired
	private ResumeManager resumeManager;

	@Ignore
	@Test
	public void saveResume() throws AppException {
		Resume p = new Resume();
		Personal pp = new Personal();
		pp.setEmail("d@a.com");
		pp.setLegalName("anil");
		pp.setCurrentLocation("Pune");
		Gender gender = new Gender();
		gender.setOutAtWork("Y");
		
		List<String> skills = new ArrayList<>();
		skills.add("Java");
		skills.add("Cloud");
		skills.add("Distributed Caching");
		p.setSkills(skills);
		p.setUserId("test4@gmail.com");
		p.setPersonal(pp);
		
		resumeManager.saveProject(p);
	}
	
	@Test
	public void searchResume() throws AppException {
		Map<String,String> map = new HashMap<>();
		//map.put("userId", "test2@gmail.com");
		map.put("personal.currentLocation", "Jamshedpur,Pune");
		List<String> strList = new ArrayList<>();
		strList.add("Java");
		map.put("skills", "Java");
		QueryParamTO paramTO = new QueryParamTO();
		paramTO.setQueryMap(map);
		List<Resume> pList =  resumeManager.searchResume(paramTO);
		System.out.println("size : " + pList.size());
		System.out.println(pList);
		assertTrue(pList.size() > 0);
	}
	
	@Test
	public void searchResumeAll() throws AppException {
		Map<String,String> map = new HashMap<>();
		QueryParamTO paramTO = new QueryParamTO();
		paramTO.setQueryMap(map);
		List<Resume> pList =  resumeManager.searchResume(paramTO);
		System.out.println("size : " + pList.size());
		System.out.println(pList);
		assertTrue(pList.size() > 0);
	}
	
	@Test
	public void testFindByUserId() throws AppException {
		
		Resume rs =  resumeManager.findByUserId("test4@gmail.com");
		System.out.println("resume : " + rs);
		assertNotNull(rs);
	}
}
